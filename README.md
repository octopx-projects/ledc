# LEDC Project - Choose London

This project is a multiplatform design web-oriented campaign focusing on captivate and attract talented people to fulfill available tech job positions in London, Ontario. Moreover, this campaign intends to bring awareness to technology companies and new entrepreneurs about the benefits of settling, building and running a business in this city.

The requirements of the first phase of this project are:  
	* Researching data and content about London and planning how to use them to produce the campaign;  
	* Creating the fundaments of the campaign and presenting how it will be built in second phase;  
	* Presenting research documents about technologies that will be used to achieve the project goal, and metrics to be used to evaluate results;  
	* Producing a detailed responsive design of the website to be built in second phase;  
	* Collecting assets and content material to  create a promotional video in second phase, showing how they it will look like by styleframes and storyboards;  
	* Following best practices of accessibility and UX/UI to construct a compliant website.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

At this point of first phase the project team is still researching and defining tools and technologies to produce the campaign, including the website. However, it is very likely that tools like SASS will be used in this project. Along the research and development, if some technology is included as prerequisite to develop, deploy or run this project, this README file will be properly updated.

### Prerequisites

### Installing

To get everything running, after cloning from the repo, run `npm install` on the root project folder.

## Running the tasks
Grunt is implemented as a task runner for this project. In order to watch changes made in the files, run `grunt` inside the project folder.
Grunt tasks include: concatenation and js uglify; run sass; image compressor; and autoprefixer through postcss.
Grunt watch for changes in js and all scss files.  


## Running the tests

Soon.


## Deployment

Soon.

## Built With

Soon

## Contributing

At this moment of this project, due to the fact that this is a school project yet, we are not allowed to receive or accept any kind of intelectual or development contribution, though we are aware that exchange of ideas and work always improve code and projects.

## Versioning

We use Bitbucket for version control.

## Authors

[**Octopx Digital**](https://bitbucket.org/octopx-projects/) is:  
	* [**Barbara Bombachini**](https://bitbucket.org/bbombachini/) - Backend Developer  
	* [**Emre Filiz**](https://bitbucket.org/emrefiliz/) - Graphic Designer  
	* [**Eric Lee**](https://bitbucket.org/elee378/) - Motion/3D Artist  
	* [**Flavia Tozzini**](https://bitbucket.org/f-tozzini/) - Frontend Developer  
	* [**Mauricio Silveira**](https://bitbucket.org/maursilveira/) - Project Manager  


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Soon.
